import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Start from './Component/start';

export default function App() {
  return (
    <View>
      <StatusBar style="auto" />
      <Start></Start>
    </View>
  );
}


