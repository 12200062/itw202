import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
const name = "Sonam Wangmo";
const Start = () => {
    return (
        <View>
            <Text style={styles.container}>Getting started with React Native!</Text>
            <Text style={styles.item}>My name is {name}</Text>
        </View>
    )
};
const styles = StyleSheet.create({
    container: {
      flex: 1,
      fontSize:45,
      backgroundColor: '#fff',
      textAlign: 'center',
      justifyContent: 'center',
    },
    item:{
        fontSize:20,
        textAlign: 'center',
    },
  });
export default Start;