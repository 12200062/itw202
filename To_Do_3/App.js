import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { add, multiply } from './Components/nameExport';
import division from './Components/defaultExport';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Hello World!</Text>
      <Text>Result of Addition: {add(5,6)}</Text>
     <Text>Result of Multiplication: {multiply(5,6)}</Text>
     <Text>Result of Division: {division(10,2)}</Text>

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
