import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.first}>
    <View style={styles.second}><Text>1</Text></View>
    <View style={styles.third}><Text>2</Text></View>
    <View style={styles.fourth}><Text>3</Text></View>
    </View>
  );
}

const styles = StyleSheet.create({
  first: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row',
    textAlign:'center', 
  },
  second: {
    backgroundColor:'red',
    width:50,
    height:150, 
    margin: 1,
    marginTop:-299,
    marginLeft:-300,
    paddingTop:70,
  },
  third: {
    backgroundColor:'blue',
    width:100,
    height:150, 
    margin: -1,
    marginTop:-300,
    paddingTop:70,
  },
  fourth: {
    backgroundColor:'green',
    width:10,
    height:150, 
    margin: -1,
    marginTop:-300,
    paddingTop:70,
  }
});
