import React, {useState} from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
const AppButton= () => {
    const [ count, setCount ] = useState(0);
    const [isDisabled, setIsDisabled] = useState(false);

    const buttonDisable = () => {
        if (count == 3) {
              setIsDisabled(true)  
        }
        else{
            setCount(count +1) 
        }
        
    }

    return (
        <View>
            <Text>
                The Button was pressed {count} times!
            </Text>
            <Button onPress={() =>buttonDisable()}
            title="Click Me!" disabled={isDisabled} style = {styles.button}/>
            
        </View>
    );
};
const styles = StyleSheet.create({
    button:{
    borderWidth: 2,
    backgroundColor:'blue'
    }
})
export default AppButton;