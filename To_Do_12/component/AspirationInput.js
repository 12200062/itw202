import React, {useState} from 'react';
import { StyleSheet, View, TextInput, Button, Modal } from 'react-native';
const Aspirationinput = (props) => {
    const [enteredAspiration, SetEnteredAspiration] = useState('');
    
    const clearInput = (enteredText) => {
        SetEnteredAspiration();
    }


    const AspirationInputHandler = (enteredText) => {
        SetEnteredAspiration(enteredText);
    };

    return (
        <Modal visible={props.visible} animationType='slide'>
        <View style={StyleSheet.inputContainer}>
            <TextInput
            placeholder='My aspiration from this module'
            style={styles.input}
            onChangeText={AspirationInputHandler}
            value={enteredAspiration} />
            <Button
            title='ADD'
            onPress={() => props.onAddAspiration(enteredAspiration)} />
            <Button title="CANCEL" onPress={clearInput} />
        </View>
        </Modal>
    );
}

export default Aspirationinput;
const styles = StyleSheet.create({
    inputContainer:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center'
      },
      input:{
        marginTop:30,
        width:'80%',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10
      }
})