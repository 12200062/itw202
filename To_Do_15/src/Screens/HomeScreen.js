import React from 'react';
import Header from '../components/Header';
import Background from '../components/Background';

export default function HomeScreen(){
    return(
        <Background>
            <Header>Home</Header>
        </Background>
    )
}