import React from 'react'
import { View, Image, StyleSheet } from 'react-native';

const ImageComponent = () => {
     return (
         <View>
            <Image style={styles.logoStretch}
             source={{uri:'https://picsum.photos/100/100'}}/>

            <Image style={styles.logo}
            source={require('../assets/Logo.png')}/>
         </View>
    )
 }
const styles = StyleSheet.create({
    logoStretch:{
        marginLeft:500,
        marginTop:100,
        height:100, 
        width:100,
    },
    logo:{
        marginLeft:500,
        marginTop:100,
        height:100,
        width:100,
    }
})
export default ImageComponent;