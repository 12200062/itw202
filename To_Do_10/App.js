import React,{useState} from 'react';
import { StyleSheet, Text, View, TextInput} from 'react-native';

export default function App() {

  const [input, setInput] = React.useState("");
 
  return (
    <View style={styles.container}>
      <Text>Hi {input}</Text>
      <Text>What is your name?</Text>
      <TextInput style={styles.text} 
      onChangeText={(text) => setInput(text)}/>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
    marginTop:10,
    borderRadius:5,
    width:250,
    backgroundColor:'lightgray',
    padding: 7,
  }
});
