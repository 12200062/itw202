import {View, StyleSheet} from 'react-native';

import { Colors } from '../../constants/Colors';
import OutlinedButton from '../UI/OutlinedButton';
import {getCurrentPositionAsync, useForegroundPermissions, PermissionStatus} from 'expo-location';

function LocationPicker() {
   async function getLocationHandler(){
      const [locationPermissionInformation, requestPermission] = 
      useForegroundPermissions();

    async function verifyPermissions() {
        if (
            locationPermissionInformation.status === PermissionStatus.UNDETERMINED
        ){
            const permissionResponse = await requestPermission();
            return permissionResponse.granted;
        }
        if (locationPermissionInformation.status === PermissionStatus.DENIED){
            Alert.alert(
                'Insufficient Permissions!',
                'You need to grant location permission to use this app.'
            );
            return false;
        }
        return true;
    }
    async function getLocationHandler(){
        const hasPermission = await verifyPermissions();
        if (!hasPermission) {
            return;
        }
        const location = await getCurrentPositionAsync();
        console.log(location);
    }
   }
    return(
        <View>
            <View style={styles.mapPreview}>
                <View style={styles.actions}>
                    <OutlinedButton icon='location' onPress={getLocationHandler}>
                        Locate User
                    </OutlinedButton>
                    <OutlinedButton icon='map' onPress={pickOnMapHandler}>
                        Pick on Map
                    </OutlinedButton>
                </View>
            </View>
        </View>
    )
}
export default LocationPicker;

const styles = StyleSheet.create({
    mapPreview: {
        width: '100%',
        height: 200,
        marginVertical: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary100,
        borderRadius: 4,

    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },

});
