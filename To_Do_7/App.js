import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.first}></View>
      <View style={styles.second}></View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  first: {
    flex: 1,
    height:10,
    width:150,
    marginTop:300,
    backgroundColor: 'red',
  },
  second: {
    flex: 1,
    height:10,
    width:150,
    marginBottom:50,
    backgroundColor: 'blue',
  },
});
