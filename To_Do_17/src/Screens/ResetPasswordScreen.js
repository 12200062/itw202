import React, {useState} from "react";
import { TouchableOpacity, StyleSheet, View, Text } from "react-native";
import Button from "../components/Button";
import Header from "../components/Header";
import Background from "../components/Background";
import BackButton from "../components/BackButton";
import Logo from "../components/Logo";
import TextInput from "../components/TextInput";
import { emailValidator } from "../core/helpers/emailValidator";
import { theme } from "../core/theme";
import { sendPasswordResetEmail } from "../api/auth-api";


export default function ResetPasswordScreen({navigation}){
    const [email,setEmail] = useState({value:"", error: ""})
    const [loading, setLoading]=useState();

    const onSubmitPressed =  async () => {
        const emailError = emailValidator(email.value); 
       
        if(emailError){
            setEmail({ ...email,error:emailError });
        }
        setLoading(true)
        const response = await sendPasswordResetEmail(email.value)
    
        if(response.error){ 
            alert(response.error)
        }
        else{
            navigation.replace('LoginScreen')
            alert("Reset Email has been sent successfully!")
        }
        setLoading(false)
    }
   
    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Restore Password</Header>
            <TextInput 
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text) => setEmail({value:text,error:""})}
                description="You will receive email with password reset link."
            />
            <Button mode = "contained" onPress={onSubmitPressed}loading={loading}>Send Instructions</Button>
        
        </Background>
    )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      width:'100%'
      
    },
    row:{
        flexDirection:'row',
        marginTop:4
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary
    }
  });